using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;
using com.faith.coreconsole;

public class #SCRIPTNAME# : CraftMaster3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();
        #NOTRIM#
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS
        #NOTRIM#
        #NOTRIM#
    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS
        #NOTRIM#
        #NOTRIM#
    #endregion ALL SELF DECLEAR FUNCTIONS

}
