﻿using UnityEngine;
using System.Collections;
using com.faith.core;
public class DraggableSoilAndPlantGameEvent : ItemDragGameEvent
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();
        
    }



    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Configuretion

    private IEnumerator ControllerForRestoringTheLastDragItem() {

        if (_selectedItemToBeDrag != null && _selectedItemToBeDrag.isAllowedToRestorePositionOnInteractionComplete) {

            WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
            float duration = 1f;
            float remaingTime = duration;
            float progression;
            while (remaingTime > 0) {

                progression = 1f - (remaingTime / duration);

                _selectedItemToBeDrag.transform.position = Vector3.Lerp(
                        _selectedItemToBeDrag.transform.position,
                        _selectedItemToBeDrag.InitialPosition,
                        progression
                    );

                _selectedItemToBeDrag.transform.rotation = Quaternion.Slerp(
                        _selectedItemToBeDrag.transform.rotation,
                        _selectedItemToBeDrag.InitialRotation,
                        progression
                    );

                yield return waitForEndOfFrame;
                remaingTime -= Time.deltaTime;
            }
        }

        StopCoroutine(ControllerForRestoringTheLastDragItem());
    }

    #endregion

    #region Override Method


    protected override void OnTouchDown(Vector3 touchPosition, int touchIndex)
    {
        
    }

    protected override void OnTouch(Vector3 touchPosition, int touchIndex)
    {
        
    }

    protected override void OnTouchUp(Vector3 touchPosition, int touchIndex)
    {
        
    }

    protected override void ResetEvent()
    {
        
    }

    protected override void StartEvent()
    {
        
    }

    protected override void EndEvent()
    {
        StartCoroutine(ControllerForRestoringTheLastDragItem());
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
