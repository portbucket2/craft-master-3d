﻿using UnityEngine;

public class DraggableSoil : ItemDragObject
{
    #region Public Variables

    public ParticleSystem soilParticle;
    public Transform soilFbx;
    public Transform growthSoil;

    #endregion

    #region Private Variables

    private Vector3 _initialLocalPosition;
    private Vector3 _initalLocalScale;

    #endregion

    #region Mono Behaviour

    protected override void Awake()
    {
        base.Awake();

        _initialLocalPosition = soilFbx.localPosition;
        _initalLocalScale = soilFbx.localScale;
    }

    #endregion

    #region Override Method

    protected override void OnDeselection()
    {
        soilParticle.Stop();
    }

    protected override void OnSelecting()
    {

    }

    protected override void OnSelection()
    {

    }

    protected override void OnEnteringInteractionArea(RaycastHit rayCastHit)
    {
        soilParticle.Play();
    }

    protected override void OnExitingInteractionArea(RaycastHit rayCastHit)
    {
        soilFbx.localPosition = _initialLocalPosition;
        soilParticle.Stop();
    }

    protected override void OnInteractionArea(RaycastHit rayCastHit)
    {
        growthSoil.localScale = Vector3.Lerp(
                Vector3.zero,
                new Vector3(0.9f, 1f, 0.9f),
                InteractionProgression
            );

        soilFbx.localPosition = Vector3.Lerp(
                _initialLocalPosition,
                _initialLocalPosition + Vector3.up * 0.0125f,
                InteractionProgression
            );

        soilFbx.localScale = Vector3.Lerp(
                _initalLocalScale ,
                new Vector3(_initalLocalScale.x * 0.5f, _initalLocalScale.y * 0.1f, _initalLocalScale.z * 0.5f),
                InteractionProgression
            );

        if (InteractionProgression >= 1 && soilParticle.isPlaying)
            soilParticle.Stop();

    }

    protected override void OnInteractionComplete(bool hasExecutedInRightOrder)
    {
        soilFbx.localScale = Vector3.zero;
        soilParticle.Stop();
    }

    #endregion


}
