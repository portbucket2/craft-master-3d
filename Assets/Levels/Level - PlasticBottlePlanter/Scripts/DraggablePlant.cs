﻿using UnityEngine;
using System.Collections;
public class DraggablePlant : ItemDragObject
{
    #region Public Variables

    [Header("Parameter  :   Plant")]
    public Transform parentForPlantAfterPlanting;
    public Rigidbody rigidbodyReference;
    public MeshCollider meshColliderReference;

    [Space(5.0f)]
    public float durationForPlanting = 0.5f;
    public Transform potedPosition;
    public Vector3 potedPositionOffset;
    public Vector3 potedPlantingPosition;

    #endregion

    #region Configuretion

    private IEnumerator ControllerForPlanting()
    {
        float progression;
        float remainingTimeForPlanting = durationForPlanting;
        WaitForEndOfFrame endOfFrame = new WaitForEndOfFrame();

        while (remainingTimeForPlanting > 0) {

            progression = 1f - (remainingTimeForPlanting / durationForPlanting);

            Vector3 position = Vector3.Lerp(
                    transform.position,
                    potedPosition.position + potedPositionOffset,
                    progression
                );

            transform.position = position;

            yield return endOfFrame;
            remainingTimeForPlanting -= Time.deltaTime;
        }

        transform.SetParent(parentForPlantAfterPlanting);

        StopCoroutine(ControllerForPlanting());
    }

    private IEnumerator ControllerForEnablePhysicsForPlant() {

        transform.SetParent(parentForPlantAfterPlanting);

        meshColliderReference.enabled = true;
        rigidbodyReference.isKinematic = false;

        yield return new WaitForSeconds(2f);

        meshColliderReference.enabled = false;
        rigidbodyReference.isKinematic = true;

        StopCoroutine(ControllerForEnablePhysicsForPlant());
    }

    #endregion

    protected override void OnDeselection()
    {
        
    }

    protected override void OnSelecting()
    {
        
    }

    protected override void OnSelection()
    {
        
    }

    protected override void OnEnteringInteractionArea(RaycastHit rayCastHit)
    {
        
    }

    protected override void OnExitingInteractionArea(RaycastHit rayCastHit)
    {
        
    }

    protected override void OnInteractionArea(RaycastHit rayCastHit)
    {
        Vector3 potPosition = potedPosition.position + potedPositionOffset;
        Vector3 position = Vector3.Lerp(
                potPosition + potedPlantingPosition,
                potPosition,
                InteractionProgression
            );
        transform.position = position;
    }

    protected override void OnInteractionComplete(bool hasExecutedInRightOrder)
    {
        if (hasExecutedInRightOrder)
        {
            StartCoroutine(ControllerForPlanting());
        }
        else {

            StartCoroutine(ControllerForEnablePhysicsForPlant());
        }
    }
}
