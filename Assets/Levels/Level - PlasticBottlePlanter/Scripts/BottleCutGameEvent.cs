﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using com.faith.core;

public class BottleCutGameEvent : DotedLineGameEvent, IBatchedUpdateHandler
{

    #region Public Variables

    public bool IsCurrentAnimationSequenceRunning { get; private set; }

    public Transform knifeTransform;
    [Range(0.1f,1f)]
    public float knifeResponseToUserInput = 0.1f;

    [Header("Parameter  :   Animator")]
    public Animator knifeAnimator;
    public Animator bottleUpperPartAnimator;
    public Animator bottleLowerPartAnimator;

    #endregion

    #region Private Variables

    private bool        _isHoveringOverDotedLineComplete;

    private bool        _isResetingKnife = false;
    private Vector3     _knifeInitialPosition;
    private Quaternion  _knifeInitialRotation;

    private Vector3     _knifePosition;
    private Vector3     _knifeLookAtPosition;

    #endregion

    #region Mono Behaviour

    protected override void Awake()
    {
        base.Awake();

        _knifeInitialPosition = knifeTransform.position;
        _knifeInitialRotation = knifeTransform.rotation;
    }

    #endregion

    #region Configuretion

    private IEnumerator ResetingKnife() {

        WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();

        while (_isResetingKnife) {

            Vector3 position = Vector3.Lerp(
                    knifeTransform.position,
                    _knifeInitialPosition,
                    0.1f
                );
            knifeTransform.position = position;

            Quaternion knifeRotation = Quaternion.Slerp(
                    knifeTransform.rotation,
                    _knifeInitialRotation,
                    0.1f
                );
            knifeTransform.rotation = knifeRotation;

            yield return waitForEndOfFrame;
        }

        StopCoroutine(ResetingKnife());
    }

    private IEnumerator ControllerForBottleCuttingSequence() {

        IsCurrentAnimationSequenceRunning = true;
        WaitUntil waitUntilAnimationSequenceEnd = new WaitUntil(() =>
        {
            if (IsCurrentAnimationSequenceRunning)
                return false;

            return true;
        });


        IsCurrentAnimationSequenceRunning = true;
        bottleUpperPartAnimator.SetTrigger(GlobalConstant.ANIMATION_SEQUENCE);
        yield return waitUntilAnimationSequenceEnd;

        IsCurrentAnimationSequenceRunning = true;
        bottleLowerPartAnimator.SetTrigger(GlobalConstant.ANIMATION_SEQUENCE);
        yield return waitUntilAnimationSequenceEnd;

        OnEndEvent.Invoke();

    }

    #endregion


    #region Override Method

    protected override void OnTouchDown(Vector3 touchPosition, int touchIndex)
    {
        if (!_isHoveringOverDotedLineComplete) {

            if (SelectedDotedLine == null) {
                SelectedDotedLine = dragPoints[0].transform;
            }

            _isResetingKnife = false;
            BatchedUpdate.Instance.RegisterToBatchedUpdate(this, 0);
        }
    }

    protected override void OnTouch(Vector3 touchPosition, int touchIndex)
    {
        if (!_isHoveringOverDotedLineComplete && SelectedDotedLine != null) {
            _knifeLookAtPosition = SelectedDotedLine.position;
            _knifePosition = _knifeLookAtPosition + Vector3.up * 0.1f;
        }
        
    }

    protected override void OnTouchUp(Vector3 touchPosition, int touchIndex)
    {
        BatchedUpdate.Instance.UnregisterFromBatchedUpdate(this);

        if (!_isHoveringOverDotedLineComplete)
        {
            _isResetingKnife = true;
            StartCoroutine(ResetingKnife());
        }
    }

    protected override void ResetEvent()
    {
        
    }

    protected override void StartEvent()
    {
        _isHoveringOverDotedLineComplete = false;
    }

    protected override void EndEvent()
    {
        gameManager.centralAnimatorForDisappear.DoAnimate(new List<Transform>() { knifeTransform });
    }

    protected override void OnCompletedHoveringOverTheDotedLine()
    {
        _isHoveringOverDotedLineComplete = true;
        StartCoroutine(ControllerForBottleCuttingSequence());   
    }

    #endregion

    #region Public Variables

    public void GoToNextAnimationSequence() {

        IsCurrentAnimationSequenceRunning = false;
    }

    public void OnBatchedUpdate()
    {
        if (!IsCurrentAnimationSequenceRunning) {

            Vector3 position = Vector3.Lerp(
                    knifeTransform.position,
                    _knifePosition,
                    knifeResponseToUserInput
                );
            knifeTransform.position = position;

            Quaternion knifeRotation = Quaternion.Slerp(
                    knifeTransform.rotation,
                    Quaternion.Euler((Vector3.up * 90) + Quaternion.LookRotation(_knifeLookAtPosition - position).eulerAngles),
                    knifeResponseToUserInput
                );
            knifeTransform.rotation = knifeRotation;
        }
    }

    #endregion
}
