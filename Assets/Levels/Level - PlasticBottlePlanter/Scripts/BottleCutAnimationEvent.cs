﻿
using UnityEngine;

public class BottleCutAnimationEvent : AnimationEvent
{
    #region Public Variables

    public BottleCutGameEvent bottleCutGameEvent;

    #endregion

    #region Abstruct Method

    public override void OnAnimationEnd()
    {
        bottleCutGameEvent.GoToNextAnimationSequence();
    }

    public override void OnAnimationStart()
    {
        
    }

    #endregion

}
