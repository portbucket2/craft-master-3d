﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;
using com.faith.coreconsole;

public class PlasticBottlePlantLevelEventController : LevelGameEventsController
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();
        
    }



    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    [Header("Parameter  :   Plastic Bottle Plant")]
    public ParticleSystem sunShaft;

    [Space(5.0f)]
    public Transform plasticBottlePlant;
    public Transform plasticBottlePreviewPosition;


    #endregion

    #region Absutrct Method

    protected override void OnAllGameEventComplete()
    {
        gameManager.centralAnimatorForDisappear.DoAnimate(
            new List<Transform>() { plasticBottlePlant },
            ()=>
            {
                if (gameManager.SharedGameData.hasComplete)
                    sunShaft.Play();

                gameManager.ChangeGameState(GameState.PreviewSetLoaded);

                plasticBottlePlant.SetParent(plasticBottlePreviewPosition);
                plasticBottlePlant.localPosition = Vector3.zero;
                gameManager.centralAnimatorForAppear.DoAnimate(
                    new List<Transform>() { plasticBottlePlant },
                    () =>
                    {
                        gameManager.ChangeGameState(GameState.LevelEnded);
                    });
            }
        );
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
