﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using com.faith.coreconsole;

public abstract class LevelGameEventsController : CraftMaster3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();
        
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    protected override void OnLevelStarted()
    {
        base.OnLevelStarted();

        StartCoroutine(ControllerForIteratingGameEvents());
    }

    protected override void OnPreviewSetLoaded()
    {
        base.OnPreviewSetLoaded();

        CameraController.Instance.SwitchCamera(previewVirtualCamera);
    }

    protected override void OnLevelEnded()
    {
        base.OnLevelEnded();


        if (gameManager.SharedGameData.hasComplete)
            confettingParticle.Play();
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Varaibles

    [SerializeField] protected CinemachineVirtualCamera previewVirtualCamera;

    [Header("Parameter  :   PreviewImage")]
    public Sprite previewSprite;

    [Header("Parameter  :   Particles")]
    [SerializeField] protected ParticleSystem confettingParticle;

    [Space(5.0f), Range(0f,5f)]
    public float            delayOnCompletingAllGameEvent;
    public List<GameEvent>  gameEvents;

    #endregion

    #region Private Variables

    private bool _forceResetCurrentEvent = false;

    #endregion

    #region Configuretion

    private bool IsValidEventIndex(int eventIndex) {
        return (eventIndex >= 0 && eventIndex < gameEvents.Count) ? true : false;
    }

    private IEnumerator PlayEventAfterResetingCurrentEvent(int eventIndex) {

        WaitUntil waitUntilTheResetOfCurrentEvent = new WaitUntil(() =>
        {
            if (!_forceResetCurrentEvent)
                return true;

            return false;
        });

        yield return waitUntilTheResetOfCurrentEvent;

        StartCoroutine(ControllerForIteratingEvent(eventIndex));

        StopCoroutine(PlayEventAfterResetingCurrentEvent(eventIndex));
    }

    private IEnumerator ControllerForIteratingEvent(int eventIndex) {

        gameEvents[eventIndex].OnStartEvent?.Invoke();

        WaitUntil waitUntilEventEnded = new WaitUntil(() =>
        {
            if (!gameEvents[eventIndex].IsEventRunning || _forceResetCurrentEvent)
                return true;

            return false;
        });
        yield return waitUntilEventEnded;

        StopCoroutine(ControllerForIteratingEvent(eventIndex));

        if (_forceResetCurrentEvent) {
            gameEvents[eventIndex].OnResetEvent?.Invoke();
            _forceResetCurrentEvent = false;
        }
    }

    private IEnumerator ControllerForIteratingGameEvents()
    {
        int numberOfGameEvent = gameEvents.Count;
        for (int i = 0; i < numberOfGameEvent; i++) {

            yield return ControllerForIteratingEvent(i);
        }

        yield return new WaitForSeconds(delayOnCompletingAllGameEvent);

        float totalGameEventScore = 0;
        for (int i = 0; i < numberOfGameEvent; i++)
            totalGameEventScore += gameEvents[i].GameEventScore;

        totalGameEventScore /= numberOfGameEvent;

        gameManager.SharedGameData.hasComplete = totalGameEventScore >= 1 ? true : false;

        OnAllGameEventComplete();

        StopCoroutine(ControllerForIteratingGameEvents());

    }

    #endregion

    #region Public Callback

    public void PlayEvent(int eventIndex, bool resetIfAnyOtherEventRunning = false) {

        if (IsValidEventIndex(eventIndex))
        {
            if (resetIfAnyOtherEventRunning)
            {
                _forceResetCurrentEvent = true;
                StartCoroutine(PlayEventAfterResetingCurrentEvent(eventIndex));
            }
            else {
                StartCoroutine(ControllerForIteratingEvent(eventIndex));
            }
        }
        else {
            CoreConsole.LogError(string.Format("Invalid EventIndex = {0}", eventIndex));
        }
    }

    #endregion

    #region Abstruct Method

    protected abstract void OnAllGameEventComplete();

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
