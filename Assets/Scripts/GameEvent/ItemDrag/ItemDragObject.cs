﻿
using UnityEngine;

public abstract class ItemDragObject : MonoBehaviour
{
    #region Public Variables

    public bool IsInteractionComplete { get { return InteractionProgression >= 1 ? true : false; } }

    public bool VanishItemOnEndOfTheEvent { get { return _vanishItemOnEndOfTheEvent; } }
    public bool ResetProgressionOnTouchUp { get { return _resetProgressionOnTouchUp; } }

    public float InteractionProgression { get; private set; }

    public Vector3 OffsetForMovingArea { get { return _offsetForMovingArea; } }
    public Vector3 InitialPosition { get; private set; }
    public Quaternion InitialRotation { get; private set; }


    [SerializeField] private bool       _vanishItemOnEndOfTheEvent;

    [Header("Parameter  :   MovingArea")]
    public bool isAllowedToRestorePositionOnInteractionComplete = true;
    [SerializeField] private Vector3    _offsetForMovingArea;
    public LayerMask layerForMovingArea;

    [Header("Parameter  :   Interaction")]
    public LayerMask layerForInteraction;
    [SerializeField] private bool _resetProgressionOnTouchUp;
    [Range(0.01f,100)]
    public float interactionProgressionSpeed = 1;

    #endregion

    #region Private Variables

    private Camera _mainCameraReference;
    private bool _hasExecutedInRightOrder;
    private bool _IsEnteredTheInteractionArea = false;
    private bool _IsInteractionCompleteCallbackHasInvoked = false;

    

    #endregion

    #region Mono Behaviour

    protected virtual void Awake()
    {
        InitialPosition = transform.position;
        InitialRotation = transform.rotation;

        _mainCameraReference = Camera.main;

    }

    #endregion

    #region Public Callback

    public void Reset()
    {
        InteractionProgression = 0;
        _IsInteractionCompleteCallbackHasInvoked = false;
    }

    public void PreProcess() {

        OnSelection();
    }

    public void PostProcess() {

        OnDeselection();
    }

    public void OnTouchPositionWhenSelectAndHold(Vector3 touchPosition, int touchIndex) {

        RaycastHit rayCastHit;
        Ray ray = _mainCameraReference.ScreenPointToRay(touchPosition);
        if (Physics.Raycast(ray, out rayCastHit, 1000, layerForInteraction))
        {

            if (!_IsEnteredTheInteractionArea)
            {
                OnEnteringInteractionArea(rayCastHit);
                _IsEnteredTheInteractionArea = true;
            }

            InteractionProgression += Time.deltaTime * interactionProgressionSpeed;
            InteractionProgression = Mathf.Clamp01(InteractionProgression);

            OnInteractionArea(rayCastHit);
        }
        else {

            if (_IsEnteredTheInteractionArea) {

                OnExitingInteractionArea(rayCastHit);
                _IsEnteredTheInteractionArea = false;
            }
        }

        OnSelecting();
    }

    public void CheckIfInteractionComplete() {

        if (IsInteractionComplete && !_IsInteractionCompleteCallbackHasInvoked)
        {
            if (VanishItemOnEndOfTheEvent)
                gameObject.SetActive(false);

            _IsInteractionCompleteCallbackHasInvoked = true;
            OnInteractionComplete(_hasExecutedInRightOrder);
        }
    }

    public void SetExecutionFlagForRightOrder(bool hasExecutedInRightOrder) {

        _hasExecutedInRightOrder = hasExecutedInRightOrder;
    }

    #endregion

    #region Abstruct Method

    /// <summary>
    /// Selected the item
    /// </summary>
    protected abstract void OnSelection();
    /// <summary>
    /// Item is selected
    /// </summary>
    protected abstract void OnSelecting();
    /// <summary>
    /// Deselected the item
    /// </summary>
    protected abstract void OnDeselection();

    /// <summary>
    /// When entered the interaction area
    /// </summary>
    protected abstract void OnEnteringInteractionArea(RaycastHit rayCastHit);
    /// <summary>
    /// On Interaction area
    /// </summary>
    protected abstract void OnInteractionArea(RaycastHit rayCastHit);
    /// <summary>
    /// When exiting from interaction area
    /// </summary>
    protected abstract void OnExitingInteractionArea(RaycastHit rayCastHit);

    /// <summary>
    /// When interaction complete
    /// </summary>
    protected abstract void OnInteractionComplete(bool hasExecutedInRightOrder);

    #endregion
}
