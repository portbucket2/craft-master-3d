﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;

public abstract class ItemDragGameEvent : GameEvent, IBatchedUpdateHandler
{
    #region Public Variables

    public bool IsSelected { get; private set; } = false;

    public LayerMask        layerOfItemCanBeDragged;

    [Space(2.5f)]
    public ItemDragObject[]   itemDragObject;

    #endregion

    #region Private Variables

    private Camera          _mainCameraReference;
    protected ItemDragObject  _selectedItemToBeDrag;


    #endregion

    #region Protected Variables

    private List<int> _indexesOfCompletedInteraction;

    #endregion

    #region Mono Behaviour

    protected override void Awake()
    {
        base.Awake();

        _mainCameraReference = Camera.main;

        OnStartEvent += () =>
        {
            _indexesOfCompletedInteraction = new List<int>();

            foreach (ItemDragObject dragObject in itemDragObject)
                dragObject.Reset();

            StartCoroutine(ControllerForAppearingDraggableItem());
        };
        OnEndEvent += () =>
        {
            BatchedUpdate.Instance.UnregisterFromBatchedUpdate(this);
            
        };

        OnTouchDownEvent    += SelectItemForDraging;
        OnTouchEvent        += DragItem;
        OnTouchUpEvent      += UnselectItemFromDraging;
    }

    #endregion

    #region Configuretion



    private IEnumerator ControllerForAppearingDraggableItem() {

        WaitForSeconds intervelBetween2DraggableItem = new WaitForSeconds(0.1f);
        int numberOfDraggableItem = itemDragObject.Length;
        for (int i = 0; i < numberOfDraggableItem; i++) {

            gameManager.centralAnimatorForAppear.DoAnimate(new List<Transform>() { itemDragObject[i].transform });

            yield return intervelBetween2DraggableItem;
        }

        StopCoroutine(ControllerForAppearingDraggableItem());
    }

    private void SelectItemForDraging(Vector3 touchPosition, int touchIndex) {

        BatchedUpdate.Instance.UnregisterFromBatchedUpdate(this);

        RaycastHit rayCastHit;
        Ray ray = _mainCameraReference.ScreenPointToRay(touchPosition);
        if (Physics.Raycast(ray, out rayCastHit, 1000, layerOfItemCanBeDragged)) {

            _selectedItemToBeDrag = rayCastHit.collider.GetComponent<ItemDragObject>();
            if (_selectedItemToBeDrag != null && !_selectedItemToBeDrag.IsInteractionComplete)
            {
                OnTouchEvent += _selectedItemToBeDrag.OnTouchPositionWhenSelectAndHold;
                _selectedItemToBeDrag.PreProcess();
                
            }
        }
    }

    private void DragItem(Vector3 touchPosition, int touchIndex) {

        if (_selectedItemToBeDrag != null) {

            RaycastHit rayCastHit;
            Ray ray = _mainCameraReference.ScreenPointToRay(touchPosition);
            if (Physics.Raycast(ray, out rayCastHit, 1000, _selectedItemToBeDrag.layerForMovingArea)) {

                if (!_selectedItemToBeDrag.IsInteractionComplete) {

                    Vector3 position = rayCastHit.point + _selectedItemToBeDrag.OffsetForMovingArea;
                    _selectedItemToBeDrag.transform.position = position;
                }
            }
        }
    }

    private void UnselectItemFromDraging(Vector3 touchPosition, int touchIndex) {

        if (_selectedItemToBeDrag != null) {

            if (!_selectedItemToBeDrag.IsInteractionComplete && _selectedItemToBeDrag.ResetProgressionOnTouchUp)
                _selectedItemToBeDrag.Reset();

            if (!_selectedItemToBeDrag.VanishItemOnEndOfTheEvent) {

                if(!_selectedItemToBeDrag.IsInteractionComplete
                || (_selectedItemToBeDrag.IsInteractionComplete && _selectedItemToBeDrag.isAllowedToRestorePositionOnInteractionComplete))
                    BatchedUpdate.Instance.RegisterToBatchedUpdate(this, 1);
            }

            OnTouchEvent -= _selectedItemToBeDrag.OnTouchPositionWhenSelectAndHold;

            if (_selectedItemToBeDrag.IsInteractionComplete)
            {
                int numberOfItemToBeDrag = itemDragObject.Length;
                for (int i = 0; i < numberOfItemToBeDrag; i++)
                {

                    if (_selectedItemToBeDrag == itemDragObject[i] && !_indexesOfCompletedInteraction.Contains(i))
                    {
                        _indexesOfCompletedInteraction.Add(i);
                        CalculateGameEventScore();

                        _selectedItemToBeDrag.SetExecutionFlagForRightOrder((i == _indexesOfCompletedInteraction.Count - 1) ? true : false);
                        
                        break;
                    }
                }
            }

            _selectedItemToBeDrag.CheckIfInteractionComplete();
            _selectedItemToBeDrag.PostProcess();

        }
    }

    public void OnBatchedUpdate()
    {
        Vector3 position = Vector3.Lerp(
                _selectedItemToBeDrag.transform.position,
                _selectedItemToBeDrag.InitialPosition,
                0.1f
            );
        _selectedItemToBeDrag.transform.position = position;

        Quaternion rotation = Quaternion.Slerp(
                _selectedItemToBeDrag.transform.rotation,
                _selectedItemToBeDrag.InitialRotation,
                0.1f
            );
        _selectedItemToBeDrag.transform.rotation = rotation;
    }

    protected override void CalculateGameEventScore()
    {
        float counter           = 0;
        int numberOfDragItem    = itemDragObject.Length;
        int numberOfCompletedInteractionWithDragItem = _indexesOfCompletedInteraction.Count;
        for (int i = 0; i < numberOfDragItem; i++)
        {
            if (i < numberOfCompletedInteractionWithDragItem) {
                if (_indexesOfCompletedInteraction[i] == i)
                    counter++;
            }
        }

        GameEventScore = counter / numberOfDragItem;

        if (numberOfDragItem == numberOfCompletedInteractionWithDragItem)
            OnEndEvent.Invoke();

        
    }

    #endregion
}
