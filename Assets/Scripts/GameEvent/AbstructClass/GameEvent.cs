﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;
using TMPro;
using com.faith.gameplay.service;

public abstract class GameEvent : CraftMaster3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        OnTouchDownEvent+= OnTouchDown;
        OnTouchEvent    += OnTouch;
        OnTouchUpEvent  += OnTouchUp;

        OnResetEvent    += PostProcess;
        OnResetEvent    += ResetEvent;
        

        OnStartEvent    += PreProcess;
        OnStartEvent    += StartEvent;
        

        OnEndEvent      += PostProcess;
        OnEndEvent      += EndEvent;
        

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public bool IsEventRunning { get; private set; }
    public float GameEventScore { get; protected set; }

    public UnityAction<Vector3, int> OnTouchDownEvent;
    public UnityAction<Vector3, int> OnTouchEvent;
    public UnityAction<Vector3, int> OnTouchUpEvent;

    public UnityAction OnResetEvent;
    public UnityAction OnStartEvent;
    public UnityAction OnEndEvent;

    [Header("Parameter  :   Camera")]
    [SerializeField] protected CinemachineVirtualCamera gameEventVirtualCameraView;

    [Header("Parameter  :   Message")]
    [SerializeField] protected string dottedLineText;
    [SerializeField] protected TextMeshProUGUI dottedLineTextField;
    [SerializeField] protected Vector2 sizeOfDottedLineText;
    [SerializeField] protected Vector3 positionOffsetOfDottedLine;

    #endregion

    #region Configuretion

    private void TouchDown(Vector3 touchPosition, int touchIndex) {

        OnTouchDownEvent?.Invoke(touchPosition, touchIndex);
    }

    private void Touch(Vector3 touchPosition, int touchIndex)
    {

        OnTouchEvent?.Invoke(touchPosition, touchIndex);
    }

    private void TouchUp(Vector3 touchPosition, int touchIndex)
    {
        OnTouchUpEvent?.Invoke(touchPosition, touchIndex);
    }

    private void PreProcess()
    {
        CameraController.Instance.SwitchCamera(gameEventVirtualCameraView);

        IsEventRunning = true;

        GlobalTouchController.Instance.EnableTouchController();

        GlobalTouchController.Instance.OnTouchDown += TouchDown;
        GlobalTouchController.Instance.OnTouch += Touch;
        GlobalTouchController.Instance.OnTouchUp += TouchUp;

        if (dottedLineTextField != null) {

            dottedLineTextField.text = dottedLineText;
            dottedLineTextField.transform.position = transform.position + positionOffsetOfDottedLine;
            dottedLineTextField.GetComponent<RectTransform>().sizeDelta = sizeOfDottedLineText;

            gameManager.centralAnimatorForUIAppear.DoAnimate(new List<Transform>() { dottedLineTextField.transform });
        }

        
    }

    private void PostProcess() {

        IsEventRunning = false;

        GlobalTouchController.Instance.OnTouchDown -= TouchDown;
        GlobalTouchController.Instance.OnTouch -= Touch;
        GlobalTouchController.Instance.OnTouchUp -= TouchUp;

        GlobalTouchController.Instance.DisableTouchController();

        if (dottedLineTextField != null) {

            gameManager.centralAnimatorForUIDisappear.DoAnimate(new List<Transform>() { dottedLineTextField.transform });
        }
    }

    #endregion

    #region Absutrct Method

    protected abstract void CalculateGameEventScore();

    protected abstract void     ResetEvent();
    protected abstract void     StartEvent();
    protected abstract void     EndEvent();

    protected abstract void OnTouchDown(Vector3 touchPosition, int touchIndex);
    protected abstract void OnTouch(Vector3 touchPosition, int touchIndex);
    protected abstract void OnTouchUp(Vector3 touchPosition, int touchIndex);

    #endregion


    #endregion ALL SELF DECLEAR FUNCTIONS

}
