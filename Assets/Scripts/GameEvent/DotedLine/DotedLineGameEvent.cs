﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faith.core;
using TMPro;

#if UNITY_EDITOR

using UnityEditor;

#endif

public abstract class DotedLineGameEvent : GameEvent
{

    #region Custom Variables

    protected enum CompletionRule
    {
        OneGo,
        Anyhow
    }

    #endregion

    #region Public Variables

#if UNITY_EDITOR
    [Header("Parameter  :   Editor")]
    public bool showPreview;
    public Color colorOfPreviewDot;
#endif

    public Transform SelectedDotedLine { get; protected set; }

    public bool HasInterseptedWithRequiredNumberOfDotedLine
    {
        get {
            int numberOfDragPoints = _interactiveDragPoints.Count;
            float counter = 0;
            for (int i = 0; i < numberOfDragPoints; i++)
            {
                if (_interactiveDragPoints[i].IsMarkedAsHited)
                    counter++;
            }

            if ((counter / numberOfDragPoints) >= accuracy)
                return true;

            return false;
        }
    }

    #endregion

    #region Protected Variables


    

    [Header("Parameter  :   Visual")]
    [SerializeField] protected LayerMask layerOfDotedLine;
    [SerializeField] protected GameObject dragPointPrefab;

    [Space(2.5f)]
    [SerializeField] protected FloatReference dotWaveLength;
    [SerializeField] protected FloatReference dotPreviewIntensity;
    [SerializeField] protected FloatReference dotSize;

    [Header("Parameter  :   Animation")]
    [Range(1f, 2.5f)]
    [SerializeField] protected float animationDuration = 1;
    [SerializeField] protected TransformAnimatorAsset appearAnimationForDotedLine;
    [SerializeField] protected TransformAnimatorAsset disappearAnimationForDotedLine;

    [Header("Parameter  :   Rules")]
    [Range(0.1f, 1f)]
    [SerializeField] protected float accuracy = 0.1f;
    [SerializeField] protected CompletionRule completionRule;

    [SerializeField] protected DotedPoint[] dragPoints;

    [SerializeField]protected List<DotedPoint> _interactiveDragPoints;

    #endregion

    #region Private Variables

    private bool _IsAbleToExecuteOnTouchUpAnimtion = false;

    private Camera          _mainCameraReference;

    #endregion

    #region Gizmos

#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        if (showPreview)
        {

            float absoluteDotPreviewIntesity = 1f - Mathf.Clamp(dotPreviewIntensity.Value, 0.05f, 0.95f);
            int numberOfDot = (int)Mathf.Clamp((1.0f / absoluteDotPreviewIntesity), 2, Mathf.Infinity);

            int numberOfDragPoints = dragPoints.Length;
            for (int i = 0; i < numberOfDragPoints - 1; i++)
            {

                int numberOfDotInThisPath = numberOfDot;
                numberOfDotInThisPath *= (int)(Mathf.Clamp(
                    Vector3.Distance(dragPoints[i].transform.position, dragPoints[i + 1].transform.position),
                    1,
                    Mathf.Infinity) / dotWaveLength.Value);

                for (int j = 0; j < numberOfDotInThisPath; j++)
                {

                    float positionInterpolatedValue = j / (float)numberOfDotInThisPath;
                    Vector3 dotPosition = Vector3.Lerp(
                            dragPoints[i].transform.position,
                            dragPoints[i + 1].transform.position,
                            positionInterpolatedValue
                        );

                    Gizmos.color = colorOfPreviewDot;
                    Gizmos.DrawWireSphere(
                            dotPosition,
                            dotSize.Value
                        );
                }
            }

            Gizmos.DrawWireSphere(
                dragPoints[numberOfDragPoints - 1].transform.position,
                dotSize.Value
            );

            Gizmos.color = Color.cyan;
            Gizmos.DrawWireCube(
                    transform.position + positionOffsetOfDottedLine,
                    sizeOfDottedLineText
                );

            Gizmos.color = default;


        }
    }

#endif



    #endregion

    #region Mono Behaviour

    protected override void Awake()
    {
        base.Awake();

        _mainCameraReference = Camera.main;

        OnStartEvent        += SpawnDragPoint;
        

        OnTouchDownEvent    += RayCastFromCameraToScreen;
        OnTouchDownEvent += (touchPosition, touchIndex) =>
        {
            _IsAbleToExecuteOnTouchUpAnimtion = false;
        };

        OnTouchEvent        += RayCastFromCameraToScreen;

        OnTouchUpEvent      += (touchPosition, touchIndex) =>
        {
            if (!_IsAbleToExecuteOnTouchUpAnimtion) {

                switch (completionRule)
                {
                    case CompletionRule.OneGo:

                        if (!HasInterseptedWithRequiredNumberOfDotedLine)
                        {

                            int numberOfInteractiveDotedPoint = _interactiveDragPoints.Count;
                            for (int i = 0; i < numberOfInteractiveDotedPoint; i++)
                            {
                                if (_interactiveDragPoints[i].IsMarkedAsHited)
                                {
                                    _interactiveDragPoints[i].SetDragPointHitStatus(false);
                                    appearAnimationForDotedLine.DoAnimate(new List<Transform>() { _interactiveDragPoints[i].transformOfDragPointMesh });
                                }
                            }

                            RecheckConnctingLine();
                        }

                        break;

                    case CompletionRule.Anyhow:

                        break;
                }

                if (HasInterseptedWithRequiredNumberOfDotedLine)
                {
                    int numberOfInteractiveDotedPoint = _interactiveDragPoints.Count;
                    for (int i = 0; i < numberOfInteractiveDotedPoint; i++)
                    {
                        if (!_interactiveDragPoints[i].IsMarkedAsHited)
                        {
                            _interactiveDragPoints[i].SetDragPointHitStatus(true);
                            disappearAnimationForDotedLine.DoAnimate(new List<Transform>() { _interactiveDragPoints[i].transformOfDragPointMesh });
                        }
                    }

                    GameEventScore = 1;

                    RecheckConnctingLine();
                    OnCompletedHoveringOverTheDotedLine();
                }

                _IsAbleToExecuteOnTouchUpAnimtion = true;
            }
        };
    }

    #endregion

    #region Configuretion

    private void SpawnDragPoint()
    {
        _interactiveDragPoints = new List<DotedPoint>();

        float absoluteDotPreviewIntesity = 1f - Mathf.Clamp(dotPreviewIntensity.Value, 0.05f, 0.95f);
        int numberOfDot = (int)Mathf.Clamp((1.0f / absoluteDotPreviewIntesity), 2, Mathf.Infinity);

        int numberOfDragPoints = dragPoints.Length;
        for (int i = 0; i < numberOfDragPoints - 1; i++)
        {

            _interactiveDragPoints.Add(dragPoints[i]);

            int numberOfDotInThisPath = numberOfDot;
            numberOfDotInThisPath *= (int)(Mathf.Clamp(
                    Vector3.Distance(dragPoints[i].transform.position, dragPoints[i + 1].transform.position),
                    1,
                    Mathf.Infinity) / dotWaveLength.Value);

            for (int j = 1; j < numberOfDotInThisPath; j++)
            {

                float positionInterpolatedValue = j / (float)numberOfDotInThisPath;
                Vector3 dotPosition = Vector3.Lerp(
                        dragPoints[i].transform.position,
                        dragPoints[i + 1].transform.position,
                        positionInterpolatedValue
                    );

                GameObject newDragPoint = Instantiate(
                        dragPointPrefab,
                        transform
                    );

                newDragPoint.transform.position = dotPosition;

                DotedPoint dragPointReference = newDragPoint.GetComponent<DotedPoint>();
                _interactiveDragPoints.Add(dragPointReference);
            }
        }
        _interactiveDragPoints.Add(dragPoints[numberOfDragPoints - 1]);

        //Drawing TheLine
        int numberOfInteractiveDragPoints = _interactiveDragPoints.Count;
        for (int i = 0; i < numberOfInteractiveDragPoints - 1; i++) {

            if (i == 0)
            {
                _interactiveDragPoints[i].SetLineRenderer(null, _interactiveDragPoints[i + 1]);
            }
            else if (i == (numberOfInteractiveDragPoints - 1))
            {
                _interactiveDragPoints[i].SetLineRenderer(_interactiveDragPoints[i - 1], null);
            }
            else {

                _interactiveDragPoints[i].SetLineRenderer(_interactiveDragPoints[i - 1], _interactiveDragPoints[i + 1]);
            }
        }

        AppearDotedLine();
    }

    private void RecheckConnctingLine() {

        int numberOfInteractiveDragPoints = _interactiveDragPoints.Count;
        for (int i = 0; i < numberOfInteractiveDragPoints; i++) {

            _interactiveDragPoints[i].DrawConnectedLine();
        }
    }

    private void RayCastFromCameraToScreen(Vector3 touchPosition, int touchIndex)
    {

        RaycastHit rayCastHit;
        Ray ray = _mainCameraReference.ScreenPointToRay(touchPosition);
        if (Physics.Raycast(ray, out rayCastHit, 1000, layerOfDotedLine))
        {

            DotedPoint dragPoint = rayCastHit.collider.GetComponent<DotedPoint>();
            if (!dragPoint.IsMarkedAsHited)
            {
                SelectedDotedLine = dragPoint.transform;

                dragPoint.SetDragPointHitStatus(true);
                disappearAnimationForDotedLine.DoAnimate(new List<Transform>() { dragPoint.transformOfDragPointMesh });

                dragPoint.DrawConnectedLine();

                if (dragPoint.PreviousDragPoint != null)
                    dragPoint.PreviousDragPoint.DrawConnectedLine();

                if (dragPoint.NextDragPoint != null)
                    dragPoint.NextDragPoint.DrawConnectedLine();

                //RecheckConnctingLine();
            }
        }
    }

    

    private IEnumerator ControllerForAppearingDotedLine()
    {
        yield return new WaitForEndOfFrame();

        int numberOfDragPoint = dragPoints.Length;
        int numberOfInteractiveDragPoints = _interactiveDragPoints.Count;

        float cycleLength = 1.0f / numberOfInteractiveDragPoints;
        WaitForSeconds cycleDelay = new WaitForSeconds(cycleLength);

        for (int i = 0; i < numberOfInteractiveDragPoints; i++)
        {
            if (i == 0) {
                _interactiveDragPoints[i].transform.rotation = Quaternion.LookRotation(_interactiveDragPoints[i + 1].transform.position - _interactiveDragPoints[i].transform.position);
            }
            else if (i == (numberOfInteractiveDragPoints - 1))
            {
                _interactiveDragPoints[i].transform.rotation = Quaternion.LookRotation(_interactiveDragPoints[i - 1].transform.position - _interactiveDragPoints[i].transform.position);
            }
            else {

                //bool isMainDragPoint = false;
                //for (int j = 0; j < numberOfDragPoint; j++) {

                //    if (dragPoints[j] == _interactiveDragPoints[i])
                //    {
                //        isMainDragPoint = true;
                //        break;
                //    }
                //}

                //if (isMainDragPoint)
                //{
                //    Vector3 eulerAngle = Quaternion.LookRotation(_interactiveDragPoints[i + 1].transform.position - _interactiveDragPoints[i].transform.position).eulerAngles + Quaternion.LookRotation(_interactiveDragPoints[i - 1].transform.position - _interactiveDragPoints[i].transform.position).eulerAngles;
                //    _interactiveDragPoints[i].transform.rotation = Quaternion.Euler(eulerAngle);
                //}
                //else {
                //    _interactiveDragPoints[i].transform.rotation = Quaternion.LookRotation(_interactiveDragPoints[i + 1].transform.position - _interactiveDragPoints[i].transform.position);
                //}

                _interactiveDragPoints[i].transform.rotation = Quaternion.LookRotation(_interactiveDragPoints[i + 1].transform.position - _interactiveDragPoints[i].transform.position);
            }

            _interactiveDragPoints[i].transform.localScale = Vector3.one * dotSize.Value;
            appearAnimationForDotedLine.DoAnimate(new List<Transform>() { _interactiveDragPoints[i].transformOfDragPointMesh });

            if (i < (numberOfInteractiveDragPoints - 1))
                yield return cycleDelay;
        }

        
    }

    #endregion

    #region Abstruct Callback

    protected abstract void OnCompletedHoveringOverTheDotedLine();

    

    #endregion

    #region Protected Callback

    protected void AppearDotedLine() {

        StartCoroutine(ControllerForAppearingDotedLine());
    }

    protected void DisappearDotedLine()
    {
        
    }

    protected override void CalculateGameEventScore()
    {

    }

    #endregion

}

#if UNITY_EDITOR

[CustomEditor(typeof(DotedLineGameEvent),true)]
public class DragGameEventEditor : Editor
{
    #region Private Variables

    private DotedLineGameEvent _reference;
    private SerializedProperty _dragPoints;

    #endregion

    #region Editor

    private void OnEnable()
    {
        _reference = (DotedLineGameEvent)target;

        if (_reference == null)
            return;

        _dragPoints = serializedObject.FindProperty("dragPoints");
    }

    private void OnSceneGUI()
    {
        int numberOfDragPoints = _dragPoints.arraySize;
        for (int i = 0; i < numberOfDragPoints; i++) {

            if (_dragPoints.GetArrayElementAtIndex(i).objectReferenceValue != null) {

                Transform dragPointTransform = ((DotedPoint)_dragPoints.GetArrayElementAtIndex(i).objectReferenceValue).transform;
                dragPointTransform.position = Handles.PositionHandle(dragPointTransform.position, dragPointTransform.rotation);
            }
            
        }
    }

    #endregion
}

#endif