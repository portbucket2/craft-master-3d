﻿
using UnityEngine;
using System.Collections;

public class DotedPoint : MonoBehaviour
{
    #region Public Variables

    public bool IsMarkedAsHited { get; private set; } = false;

    public DotedPoint PreviousDragPoint { get; private set; }
    public DotedPoint NextDragPoint { get; private set; }

    public Transform transformOfDragPointMesh;

    [Space(2.5f)]
    public float animationDuration = 1;
    public LineRenderer lineRendererPrevious;
    public LineRenderer lineRendererNext;

    #endregion

    #region Private Variables

    private bool _isLineRendererAnimationControllerRunning = false;
    private float _remainingTimeForAnimationDuration;

    

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        
    }

    #endregion

    #region Configuretion

    private IEnumerator AnimationControllerForLineRenderer() {

        float           cycleLength = 0.0167f;
        WaitForSeconds  cycleDelay  = new WaitForSeconds(cycleLength);
        while (_remainingTimeForAnimationDuration > 0) {

            float progression = 1f - (_remainingTimeForAnimationDuration / animationDuration);

            if (IsMarkedAsHited)
            {
                if (PreviousDragPoint != null)
                {
                    Vector3 position = Vector3.Lerp(
                            PreviousDragPoint.transform.position,
                            transform.position,
                            progression
                        );
                    lineRendererPrevious.SetPosition(1, position);
                }

                if (NextDragPoint != null )
                {

                    Vector3 position = Vector3.Lerp(
                            NextDragPoint.transform.position,
                            transform.position,
                            progression
                        );
                    lineRendererNext.SetPosition(1, position);
                }
            }
            else {

                if (PreviousDragPoint != null && !PreviousDragPoint.IsMarkedAsHited) {

                    Vector3 position = Vector3.Lerp(
                            transform.position,
                            PreviousDragPoint.transform.position,
                            progression
                        );
                    lineRendererPrevious.SetPosition(1, position);
                }

                if (NextDragPoint != null && !NextDragPoint.IsMarkedAsHited)
                {

                    Vector3 position = Vector3.Lerp(
                            transform.position,
                            NextDragPoint.transform.position,
                            progression
                        );
                    lineRendererNext.SetPosition(1, position);
                }
            }

            _remainingTimeForAnimationDuration -= cycleLength;
            yield return cycleDelay;
        }

        StopCoroutine(AnimationControllerForLineRenderer());
        _isLineRendererAnimationControllerRunning = false;
    }

    #endregion

    #region Public Callback

    public void SetDragPointHitStatus(bool status) {
        IsMarkedAsHited = status;
    }

    public void SetLineRenderer(DotedPoint previousDragPoint, DotedPoint nextDragPoint) {

        PreviousDragPoint  = previousDragPoint;
        NextDragPoint      = nextDragPoint;

        lineRendererPrevious.positionCount = 2;
        lineRendererPrevious.SetPosition(0, transform.position);
        lineRendererPrevious.SetPosition(1, transform.position);

        lineRendererNext.positionCount = 2;
        lineRendererNext.SetPosition(0, transform.position);
        lineRendererNext.SetPosition(1, transform.position);

        DrawConnectedLine();
    }

    public void DrawConnectedLine() {

        if (!_isLineRendererAnimationControllerRunning) {

            _isLineRendererAnimationControllerRunning = true;
            _remainingTimeForAnimationDuration = animationDuration;
            StartCoroutine(AnimationControllerForLineRenderer());
        }

        //if (IsMarkedAsHited)
        //{
        //    lineRendererPrevious.positionCount = 0;
        //    lineRendererNext.positionCount = 0;
        //}
        //else {
        //    if (_previousDragPoint != null && !_previousDragPoint.IsMarkedAsHited)
        //    {
        //        lineRendererPrevious.positionCount = 2;
        //        lineRendererPrevious.SetPosition(0, transform.position);
        //        lineRendererPrevious.SetPosition(1, _previousDragPoint.transform.position);

        //    }
        //    else
        //    {
        //        lineRendererPrevious.positionCount = 0;
        //    }

        //    if (_nextDragPoint != null && !_nextDragPoint.IsMarkedAsHited)
        //    {

        //        lineRendererNext.positionCount = 2;
        //        lineRendererNext.SetPosition(0, transform.position);
        //        lineRendererNext.SetPosition(1, _nextDragPoint.transform.position);

        //    }
        //    else
        //    {
        //        lineRendererNext.positionCount = 0;
        //    }
        //}

        
    }

    #endregion
}
