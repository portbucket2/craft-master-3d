﻿using UnityEngine;

public abstract class AnimationEvent : MonoBehaviour
{
    public abstract void OnAnimationStart();
    public abstract void OnAnimationEnd();
}
