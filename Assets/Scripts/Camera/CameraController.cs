﻿
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using com.faith.coreconsole;

#if UNITY_EDITOR

using UnityEditor;

#endif

public class CameraController : CraftMaster3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        Instance = this;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    protected override void OnPreviewSetLoaded()
    {
        base.OnPreviewSetLoaded();
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

#if UNITY_EDITOR

    [SerializeField] private int _virtualCameraIndex = 0;

#endif

    public static CameraController Instance;

    public List<CinemachineVirtualCamera> listOfCinemachineVirtualCameras;

    #endregion


    #region Public Callback

    public void SwitchCamera(CinemachineVirtualCamera virtualCamera) {

        int cameraIndex = -1;
        int numberOfVirtualCamera = listOfCinemachineVirtualCameras.Count;
        for (int i = 0; i < numberOfVirtualCamera; i++) {

            if (listOfCinemachineVirtualCameras[i] == virtualCamera) {

                cameraIndex = i;
                break;
            }
        }

        SwitchCamera(cameraIndex);
    }

    public void SwitchCamera(int cameraIndex) {

        if (cameraIndex == -1)
        {
            CoreConsole.LogError(string.Format("Invalid 'VirtualCameraIndex' / No such 'VirtualCamera' was found in the list"));
            return;
        }
        else {

            int numberOfVirtualCamera = listOfCinemachineVirtualCameras.Count;
            for (int i = 0; i < numberOfVirtualCamera; i++) {

                if (listOfCinemachineVirtualCameras[i] != null) {
                    if (i == cameraIndex)
                        listOfCinemachineVirtualCameras[i].Priority = 11;
                    else
                        listOfCinemachineVirtualCameras[i].Priority = 10;
                }
            }
        }
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}

#if UNITY_EDITOR

[CustomEditor(typeof(CameraController))]
public class CameraControllerEditor : Editor
{
    #region Private Variables

    private CameraController _reference;

    private SerializedProperty _virtualCameraIndex;
    private SerializedProperty _listOfCinemachineVirtualCameras;

    private GUIStyle _cameraLabelStyle;

    #endregion

    #region Editor

    private void OnEnable()
    {
        _reference = (CameraController)target;

        if (_reference == null)
            return;

        _virtualCameraIndex = serializedObject.FindProperty("_virtualCameraIndex");
        _listOfCinemachineVirtualCameras = serializedObject.FindProperty("listOfCinemachineVirtualCameras");

        _cameraLabelStyle = EditorStyles.boldLabel;
        _cameraLabelStyle.alignment = TextAnchor.MiddleCenter;

        _virtualCameraIndex.intValue = Mathf.Clamp(_virtualCameraIndex.intValue, 0, _reference.listOfCinemachineVirtualCameras.Count - 1);
        _virtualCameraIndex.serializedObject.ApplyModifiedProperties();
    }

    

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        CameraSwitchGUI();
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(_listOfCinemachineVirtualCameras);

        serializedObject.ApplyModifiedProperties();
    }

    #endregion

    #region CustomGUI

    private void CameraSwitchGUI() {

        EditorGUILayout.BeginHorizontal();
        {
            if (_reference.listOfCinemachineVirtualCameras.Count > 0) {

                if (GUILayout.Button("<-", GUILayout.Width(50)))
                {
                    _virtualCameraIndex.intValue = (_virtualCameraIndex.intValue - 1);
                    _virtualCameraIndex.serializedObject.ApplyModifiedProperties();
                    if (_virtualCameraIndex.intValue < 0)
                    {
                        _virtualCameraIndex.intValue = _reference.listOfCinemachineVirtualCameras.Count - 1;
                        _virtualCameraIndex.serializedObject.ApplyModifiedProperties();
                    }
                    _reference.SwitchCamera(_virtualCameraIndex.intValue);
                }

                _cameraLabelStyle = EditorStyles.boldLabel;
                _cameraLabelStyle.alignment = TextAnchor.MiddleCenter;
                EditorGUILayout.LabelField(
                    _reference.listOfCinemachineVirtualCameras[_virtualCameraIndex.intValue] == null ? "Null" : _reference.listOfCinemachineVirtualCameras[_virtualCameraIndex.intValue].name,
                    _cameraLabelStyle);

                if (GUILayout.Button("->", GUILayout.Width(50)))
                {
                    _virtualCameraIndex.intValue = (_virtualCameraIndex.intValue + 1);
                    _virtualCameraIndex.serializedObject.ApplyModifiedProperties();
                    if (_virtualCameraIndex.intValue >= _reference.listOfCinemachineVirtualCameras.Count)
                    {
                        _virtualCameraIndex.intValue = 0;
                        _virtualCameraIndex.serializedObject.ApplyModifiedProperties();
                    }
                    _reference.SwitchCamera(_virtualCameraIndex.intValue);
                }
            }
        }
        EditorGUILayout.EndHorizontal();
    }

    #endregion
}

#endif
