﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIMainMenuController : UIBaseClassForMenuController
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        startButton.onClick.AddListener(() =>
        {
            DisappearMenu();
        });
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public Button startButton;

    #endregion

    #region Abstruct Method

    protected override void OnAppearMenu()
    {
        gameManager.centralAnimatorForUIAppear.DoAnimate(
                new List<Transform>()
                {
                    startButton.transform
                }
            );
    }

    protected override void OnDisappearMenu()
    {
        gameManager.centralAnimatorForUIDisappear.DoAnimate(
            new List<Transform>()
            {
                startButton.transform
            },
            ()=> {
                rootObject.SetActive(false);
                gameManager.ChangeGameState(GameState.LevelStarted);
            });
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
