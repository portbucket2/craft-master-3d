﻿
public class UIController : CraftMaster3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        Instance = this;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    public static UIController Instance;

    public UIMainMenuController UIMainMenuControllerReference;
    public UIGameplayMenuController UIGameplayMenuControllerRefrence;

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
