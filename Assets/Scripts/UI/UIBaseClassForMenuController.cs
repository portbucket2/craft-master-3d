﻿using UnityEngine;

public abstract class UIBaseClassForMenuController : CraftMaster3DBehaviour
{
    #region Public Variables

    [Header("Parameter  :   Default Canvas Parameter")]
    [SerializeField] protected GameObject rootObject;

    #endregion

    #region Public Callback

    public void AppearMenu() {

        rootObject.SetActive(true);
        OnAppearMenu();
    }

    public void DisappearMenu() {

        OnDisappearMenu();
    }

    #endregion

    #region Abstruct Class

    protected abstract void OnAppearMenu();
    protected abstract void OnDisappearMenu();

    #endregion
}
