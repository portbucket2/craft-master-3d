﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIGameplayMenuController : UIBaseClassForMenuController
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();

        retryButton.onClick.AddListener(OnPressingRetryButton);
        nextButtonOnFailed.onClick.AddListener(OnPressingNextButton);
        nextButtonOnSuccess.onClick.AddListener(OnPressingNextButton);
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    protected override void OnLevelStarted()
    {
        base.OnLevelStarted();

        OnAppearMenu();

        Sprite previewSprite = gameManager.levelGameEventControllerReference.previewSprite;
        if (previewSprite != null)
        {
            previewImage.sprite = previewSprite;
            gameManager.centralAnimatorForUIAppear.DoAnimate(new List<Transform>() { previewPanel });
        }
    }

    protected override void OnPreviewSetLoaded()
    {
        base.OnPreviewSetLoaded();

        Sprite previewSprite = gameManager.levelGameEventControllerReference.previewSprite;
        if (previewSprite != null)
        {
            previewImage.sprite = previewSprite;
            gameManager.centralAnimatorForUIDisappear.DoAnimate(new List<Transform>() { previewPanel });
        }
    }

    protected override void OnLevelEnded()
    {
        base.OnLevelEnded();

        int levelIndex = gameManager.levelData.GetLevelIndex;

        rootObjectOfLevelEndPanel.SetActive(true);

        if (gameManager.SharedGameData.hasComplete)
        {
            levelEndText.text = string.Format("Craft Sold for {0}$", gameManager.levelData.levelInformations[levelIndex].levelSuccessValue.Value);
            gameManager.centralAnimatorForUIAppear.DoAnimate(new List<Transform>() { panelForLevelSuccess });
        }
        else {
            levelEndText.text = string.Format("Craft Sold for {0}$", gameManager.levelData.levelInformations[levelIndex].levelFailValue.Value);
            gameManager.centralAnimatorForUIAppear.DoAnimate(new List<Transform>() { panelForLevelFailed });
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variables

    [Header("Parameter  :   LevelEndPanel")]
    public GameObject   rootObjectOfLevelEndPanel;

    [Space(2.5f)]
    public Transform previewPanel;
    public Image previewImage;

    [Space(2.5f)]
    public TextMeshProUGUI levelEndText;
    public Transform    panelForLevelSuccess;
    public Transform    panelForLevelFailed;

    [Space(2.5f)]
    public Button retryButton;
    public Button nextButtonOnFailed;
    public Button nextButtonOnSuccess;

    #endregion

    #region Configuretion

    private void OnPressingNextButton() {

        gameManager.levelData.GotToNextLevel();
        OnPressingRetryButton();
    }

    private void OnPressingRetryButton() {

        int levelIndex = gameManager.levelData.GetLevelIndex;
        gameManager.LoadScene(gameManager.levelData.levelInformations[levelIndex].levelScene);
    }

    #endregion

    #region Abstruct Method

    protected override void OnAppearMenu()
    {
        rootObject.SetActive(true);

        
    }

    protected override void OnDisappearMenu()
    {
        //throw new System.NotImplementedException();
    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS

}
