﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public abstract class CraftMaster3DBehaviour : MonoBehaviour
{
    #region Private Variables

    private bool hasRegistered = false;

    [HideInInspector] public GameManager gameManager;

    #endregion

    #region Public Callback



    public void Register()
    {
        if (!hasRegistered)
        {

            gameManager = GameManager.Instance;

            gameManager.gameStateController.OnDataLoaded += OnDataLoaded;
            gameManager.gameStateController.OnSceneLoaded += OnSceneLoaded;
            gameManager.gameStateController.OnLevelStarted += OnLevelStarted;
            gameManager.gameStateController.OnPreviewSetLoaded += OnPreviewSetLoaded;
            gameManager.gameStateController.OnLevelEnded += OnLevelEnded;
            gameManager.gameStateController.OnStateChanged += OnStateChanged;



            hasRegistered = true;
        }
    }

    public void Unregister()
    {

        if (hasRegistered)
        {
            gameManager.gameStateController.OnDataLoaded -= OnDataLoaded;
            gameManager.gameStateController.OnSceneLoaded -= OnSceneLoaded;
            gameManager.gameStateController.OnLevelStarted -= OnLevelStarted;
            gameManager.gameStateController.OnPreviewSetLoaded -= OnPreviewSetLoaded;
            gameManager.gameStateController.OnLevelEnded -= OnLevelEnded;
            gameManager.gameStateController.OnStateChanged -= OnStateChanged;


            hasRegistered = false;
        }
    }

    #endregion

    #region MonoBehaviour

    protected virtual void Awake()
    {


    }

    protected virtual void OnEnable()
    {
        Register();
    }

    protected virtual void OnDisable()
    {
        Unregister();
    }

    protected virtual void OnDestroy()
    {

        Unregister();
    }

    #endregion

    #region VirtualFunction

    protected virtual void OnDataLoaded()
    {


    }

    protected virtual void OnSceneLoaded()
    {


    }

    protected virtual void OnLevelStarted()
    {


    }

    protected virtual void OnPreviewSetLoaded()
    {


    }

    protected virtual void OnLevelEnded()
    {


    }

    protected virtual void OnStateChanged(GameState gameState)
    {


    }

    #endregion

    #region Protected callback

    protected void UIAppear(Transform _transformReference, UnityAction OnTransitionEnd = null)
    {

        gameManager.centralAnimatorForUIAppear.DoAnimate(
                new List<Transform>() { _transformReference },
                OnTransitionEnd
            );
    }

    protected void UIDisappear(Transform _transformReference, UnityAction OnTransitionEnd = null)
    {

        gameManager.centralAnimatorForUIDisappear.DoAnimate(
                new List<Transform>() { _transformReference },
                OnTransitionEnd
            );
    }

    protected void UIInteract(Transform _transformReference, UnityAction OnTransitionEnd = null)
    {

        gameManager.centralAnimatorForUIInteract.DoAnimate(
                new List<Transform>() { _transformReference },
                OnTransitionEnd
            );
    }

    #endregion
}
