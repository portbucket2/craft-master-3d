﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    #region Private Variables

    private GameManager _reference;

    private SerializedProperty _listOfSplateroBehaviour;
    private SerializedProperty _includeDisabledObjectForFindingSplateroBehaviour;

    #endregion

    #region Editor

    public void OnEnable()
    {
        _reference = (GameManager) target;

        if (_reference == null)
            return;

        _listOfSplateroBehaviour = serializedObject.FindProperty("_listOfIceCreamRunBehaviour");
        _includeDisabledObjectForFindingSplateroBehaviour = serializedObject.FindProperty("_includeDisabledObjectForFindingIceCreamRunBehaviour");
    }

    public override void OnInspectorGUI()
    {

        serializedObject.Update();

        EditorGUILayout.BeginHorizontal(GUI.skin.box);
        {
            if (GUILayout.Button("Find All"))
            {
                FindAllSplateroBehaviourOnScene();
            }
        }
        EditorGUILayout.EndHorizontal();
        

        CoreEditorModule.DrawHorizontalLine();
        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }

    #endregion

    #region Configuretion

    private void FindAllSplateroBehaviourOnScene() {

        List<CraftMaster3DBehaviour> listOfIceCreamRunBehaviour = new List<CraftMaster3DBehaviour>();

        
        GameObject[] rootObjects    = SceneManager.GetActiveScene().GetRootGameObjects();
        int numberOfRootObject      = rootObjects.Length;
        for (int i = 0; i < numberOfRootObject; i++) {

            listOfIceCreamRunBehaviour.AddRange(rootObjects[i].GetComponentsInChildren<CraftMaster3DBehaviour>(_includeDisabledObjectForFindingSplateroBehaviour.boolValue));
        }

        int numberOfRailShooterBehaviour = listOfIceCreamRunBehaviour.Count;
        _listOfSplateroBehaviour.arraySize = numberOfRailShooterBehaviour;
        _listOfSplateroBehaviour.GetArrayElementAtIndex(0).objectReferenceValue = _reference;
        _listOfSplateroBehaviour.GetArrayElementAtIndex(0).serializedObject.ApplyModifiedProperties();

        for (int i = 0, j = 1; i < numberOfRailShooterBehaviour; i++) {

            if (_reference != listOfIceCreamRunBehaviour[i]) {

                _listOfSplateroBehaviour.GetArrayElementAtIndex(j).objectReferenceValue = listOfIceCreamRunBehaviour[i];
                _listOfSplateroBehaviour.GetArrayElementAtIndex(j).serializedObject.ApplyModifiedProperties();
                j++;
            }
        }

        _listOfSplateroBehaviour.serializedObject.ApplyModifiedProperties();

        EditorUtility.SetDirty(target);
    }

    #endregion
}
