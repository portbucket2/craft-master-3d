﻿public enum GameState
{
    None,
    DataLoaded,
    SceneLoaded,
    LevelStarted,
    PreviewSetLoaded,
    LevelEnded
}

public enum TileType
{
    StartTile,
    MidTile,
    EndTile,
    FridgeTile
}

public enum CollectableType
{
    Neutral,
    Buff,
    Debuff
}
