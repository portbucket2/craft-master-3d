﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.faith.core;
using com.faith.gameplay;

[DefaultExecutionOrder(Constant.EXECUTION_ORDER_GameManager)]
public class GameManager : CraftMaster3DBehaviour
{
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    protected override void Awake()
    {
        base.Awake();
        Application.targetFrameRate = 9001;
        Initialization();
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    protected override void OnLevelStarted()
    {
        base.OnLevelStarted();

        SharedGameData.hasComplete = false;
    }

    protected override void OnLevelEnded()
    {
        base.OnLevelEnded();

    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    #region Public Variable

    public static GameManager Instance;


    public GameState CurrentGameState
    {
        get { return gameStateController.gameState; }
    }

    public GameState PreviousGameState
    {
        get { return gameStateController.GetPreviousStateFromStack(); }
    }

    public SharedData SharedGameData
    {
        get { return levelData.sharedGameData; }
    }

    [Header("Reference  :   GameFlow")]
    public GameStateControllerAsset gameStateController;
    public LevelDataAsset           levelData;

    [Header("Reference  :   ServiceLocator")]
    public TimeController timeControllerReference;
    public LevelGameEventsController levelGameEventControllerReference;

    [Header("Parameter  :   SceneLoad")]
    public float initialDelayForSceneLoaded;

    [Header("Parameter  :   UIAnimation")]
    public TransformAnimatorAsset centralAnimatorForUIAppear;
    public TransformAnimatorAsset centralAnimatorForUIDisappear;
    public TransformAnimatorAsset centralAnimatorForUIInteract;

    [Header("Parameter  :   Animation")]
    public TransformAnimatorAsset centralAnimatorForAppear;
    public TransformAnimatorAsset centralAnimatorForDisappear;

    [Header("Reference  :   SplateroBehaviour")]
#if UNITY_EDITOR
    [SerializeField] private bool _includeDisabledObjectForFindingIceCreamRunBehaviour;
#endif
    [SerializeField] private List<CraftMaster3DBehaviour> _listOfIceCreamRunBehaviour;

    #endregion

    #region Private Variables

    private static bool _isInitialDataLoaded = false;

    #endregion

    #region Configuretion

    private void Initialization()
    {
        Instance = this;

        foreach (CraftMaster3DBehaviour iceCreamRunBehaviour in _listOfIceCreamRunBehaviour)
        {
            iceCreamRunBehaviour.Register();
        }

        if (!_isInitialDataLoaded)
        {

            levelData.Initialization(delegate {

                gameStateController.ChangeGameState(GameState.DataLoaded);
                gameStateController.ChangeGameState(GameState.SceneLoaded);
                _isInitialDataLoaded = true;
            });
        }
    }

    #endregion

    #region PublicCallback

    public void ChangeGameState(GameState gameState)
    {
        gameStateController.ChangeGameState(gameState);
    }

    public void LoadScene(
        SceneReference sceneReference,
        UnityAction OnSceneLoaded = null,
        float initialDelayToInvokeOnSceneLoaded = -1)
    {

        initialDelayToInvokeOnSceneLoaded = initialDelayToInvokeOnSceneLoaded == -1 ? initialDelayForSceneLoaded : initialDelayToInvokeOnSceneLoaded;

        sceneReference.LoadScene(
            OnUpdatingProgression: null,
            OnSceneLoaded: () => {
                gameManager.ChangeGameState(GameState.SceneLoaded);
                OnSceneLoaded?.Invoke();
            },
            initalDelayToInvokeOnSceneLoaded: initialDelayToInvokeOnSceneLoaded);


    }

    #endregion

    #endregion ALL SELF DECLEAR FUNCTIONS
}
